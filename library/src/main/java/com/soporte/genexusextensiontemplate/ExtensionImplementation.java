package com.soporte.genexusextensiontemplate;

import android.support.annotation.NonNull;
import android.widget.Toast;

import com.artech.actions.ApiAction;
import com.artech.activities.ActivityHelper;
import com.artech.externalapi.ExternalApi;
import com.artech.externalapi.ExternalApiResult;

import java.util.List;

public class ExtensionImplementation extends ExternalApi {

    public static String NAME = "GeneXusExtension"; //External Object name
    private static final String mMethod = "MyMethod"; //Method name in External Object
    private static final int mMethodArgsCount = 1;

    public ExtensionImplementation(ApiAction action) {
        super(action);
        addMethodHandler(mMethod, mMethodArgsCount, mMethodInvoker);
    }

    private final IMethodInvoker mMethodInvoker = new IMethodInvoker() {
        @NonNull
        @Override
        public ExternalApiResult invoke(List<Object> list) {
            //TODO: Implement MyMethod code here
            final String message = (String) list.get(0);
            ActivityHelper.getCurrentActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show(); //Example action
                }
            });
            return ExternalApiResult.SUCCESS_CONTINUE;
        }
    };
}

