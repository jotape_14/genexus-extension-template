package com.soporte.genexusextensiontemplate;

import android.content.Context;
import android.util.Log;

import com.artech.externalapi.ExternalApiDefinition;
import com.artech.externalapi.ExternalApiFactory;
import com.artech.framework.GenexusModule;

public class ExtensionModule implements GenexusModule {
    @Override
    public void initialize(Context context) {
        ExternalApiFactory.addApi(new ExternalApiDefinition(ExtensionImplementation.NAME, ExtensionImplementation.class));
        Log.d("GeneXus Extension", ExtensionImplementation.NAME + " module has been initialized.");
    }
}
